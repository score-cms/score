# Score bundles #

SAZP CMS

## What is this repository for? ##

* Score CSM
* Version: __2021.11__

### Parts of Score CMS ###

* [base](https://bitbucket.org/score-cms/base/src/2021.11/README.md "README")
* [block](https://bitbucket.org/score-cms/block/src/2021.11/README.md "README")
* [page](https://bitbucket.org/score-cms/page/src/2021.11/README.md "README")
* [cms](https://bitbucket.org/score-cms/cms/src/2021.11/README.md "README")

## How do I get set up? ##

### Option 1: ###

* Clone, install and customize this repository. (You need to create database, migration, load fixtures and change git remote)

### Option 2: ###

* Create project 4.4
    ```
     composer create-project symfony/skeleton:"4.4.*" my_project_name
    ```

* Add .htaccess to `public/` dir. - [example](https://gist.github.com/Guibzs/a3e0b3ea4eb00c246cda66994defd8a4 "GitHub")


* Add repositories to `composer.json`

    ``` 
        "repositories": [
            {
              "type": "vcs",
              "url": "git@bitbucket.org:score-cms/base.git"
            },
            {
              "type": "vcs",
              "url": "git@bitbucket.org:score-cms/block.git"
            },
            {
              "type": "vcs",
              "url": "git@bitbucket.org:score-cms/page.git"
            },
            {
              "type": "vcs",
              "url": "git@bitbucket.org:score-cms/cms.git"
            }
        ]
    ```


* Add score bundles in this order (for actual version)
    ```
    composer require score/base
    composer require score/block
    composer require score/page
    composer require score/cms
    ```
* Add routes to `config/routes/annotations.yaml`
    ```
    score_base:
        resource: "@ScoreBaseBundle/Controller/"
        prefix: /
        type: annotation
    
    score_cms:
        resource: "@ScoreCmsBundle/Controller/"
        prefix: /
        type: annotation
    
    score_block:
        resource: "@ScoreBlockBundle/Controller/"
        prefix: /
        type: annotation
    
    score_page:
        resource: "@ScorePageBundle/Controller/"
        prefix: /
        type: annotation
    ```

* Add global variables to `config/packages/twig.yaml`

    ```
    twig:
        form_themes: ['bootstrap_3_layout.html.twig']
        globals:
            lang: 'sk'
            version: '2021.11'
            reactAppLoader: "@reactAppLoader"
    ```

* If needed update `DATABASE_URL` in `.env` file and run
    ```
    php bin/console doctrine:database:create
    php bin/console make:migration
    php bin/console doctrine:migrations:migrate
    ```
* Update security settings `config/packages/security.yaml` (you can specify algorithm)

    ```  
    security:
        encoders:
            Score\CmsBundle\Entity\User: bcrypt
        providers:
            cms_provider:
                entity:
                    class: ScoreCmsBundle:User        
        firewalls:
            admin:
                anonymous: lazy
                provider: cms_provider
                pattern: ^/admin
                form_login:
                    login_path:  /admin/login
                    check_path:  /admin/login
                logout:
                    path: /admin/logout
                    target: /admin/login
  
        access_control:
            - { path: ^/admin/login, roles: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/admin, roles: ROLE_ADMIN }

    ```

* Move all files from `vendor/score/base/src/DataFixtures/` to `src/DataFixtures/` (remove original) and run (then type [yes])
    ```
    php bin/console doctrine:fixtures:load
    ```

* Change language in `config/packages/translation.yml` from `en` to `sk` (messages are in ScoreBase bundle)

    ```
    framework:
        default_locale: sk
        translator:
            default_path: '%kernel.project_dir%/translations'
            fallbacks:
                - sk
    ```   


* Now it should work. Go to `/admin/login` with  `admin/admin`


* Install web profiler for better dev options.

    ```
    composer require --dev symfony/profiler-pack
    ```




### Author ###

* Marek Hubáček

#### Coworker ####

* Jozef Hort
