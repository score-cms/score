<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Score\PageBundle\Services\PageManager;
use Score\BaseBundle\Services\AdjacencyArrayTreeManager;

class PublicController extends AbstractController
{
    /**
     * @Route("/", name="homepage-custom")
     * @Route("/{seoid}", name="subpage-custom", requirements={"seoid"="^(?!admin|login$|logout$|user/|api/|clanky).+"})
     */
    public function customSubpageAction(PageManager $pageManager, $seoid = null)
    {
        $responseCode = 200;
        if ($seoid) {
            if ($seoid === "homepage") {
                return $this->redirectToRoute('homepage-custom');
            }
            $page = $pageManager->getPageBySeoId($seoid);
            if (!$page) {
                $page = $pageManager->getPageBySeoId('error');
                $responseCode = 404;
            }
        } else {
            $page = $pageManager->getPageBySeoId('homepage');
        }

        return new Response($this->renderView('idsk/subpage.html.twig', [
            'page' => $page
        ]), $responseCode);

//        return new Response($this->renderView('@ScorePage/Public/subpage.html.twig', [
//            'page' => $page,
//            'template' => 'customLayout.html.twig'
//        ]), $responseCode);
    }
}
