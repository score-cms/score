<?php

//namespace Score\BaseBundle\DataFixtures;
namespace App\DataFixtures;

use DateTime;
use Score\CmsBundle\Entity\Form\Webform;
use Score\CmsBundle\Entity\Form\WebformField;
use Score\CmsBundle\Entity\Menu;
use Score\PageBundle\Entity\Page;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Score\BlockBundle\Entity\Block;
use Score\CmsBundle\Entity\Article;
use Score\PageBundle\Entity\PageBlock;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        //default content block
        $block = new Block();
        $block->setName('Obsah stránky');
        $block->setType('pageContent');
        $block->setVisibility(true);
        $manager->persist($block);

        //article block
        $articleBlock = new Block();
        $articleBlock->setName('Výpis článkov');
        $articleBlock->setType('controllerContent');
        $articleBlock->setModule('Score\CmsBundle\Controller\ArticleBlockController');
        $articleBlock->setAction('topAction');
        $articleBlock->setVisibility(true);
        $manager->persist($articleBlock);

        //article
        $article = new Article();
        $article->setName('CORONA INFO: Minister ohlasuje uvoľnenie pravidiel. Kaderníctva nad 1500 m nadmorskej výšky  sa otvoria pre plešatých.');
        $article->setIcon('/idsk/img/default-image.png');
        $article->setTeaser('<p>s been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a ty</p>');
        $article->setBody('<p>s been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets conta</p>  <p>s been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets conta</p>  <p>s been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets conta</p>  <p>s been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets conta</p>  <p>&nbsp;</p>');
        $article->setPublished(1);
        $article->setPriority(20);
        $article->setAuthor('George Attila Kováč');
        $article->setPublishedFrom(new \DateTime());
        $article->setPublishedTo(new \DateTime('2030-01-01'));
        $article->setSlug('article-1');
        $article->setCreatedBy(1);
        $manager->persist($article);

        $rootPage = new Page();
        $rootPage->setId(1);
        $rootPage->setName('root');
        $rootPage->setTitle('root');
        $rootPage->setLvl(0);
        $rootPage->setLang('sk');
        $manager->persist($rootPage);

        $contactPage = new Page();
        $contactPage->setId(2);
        $contactPage->setName('Kontakt');
        $contactPage->setTitle('Kontakt');
        $contactPage->setLvl(1);
        $contactPage->setSeoId('kontakt');
        $contactPage->setContent('Kontakt');
        $contactPage->setStatus('public');
        $contactPage->setLayout('menu-center');
        $contactPage->setVisibility(true);
        $contactPage->setParentPage($rootPage);
        $manager->persist($contactPage);

        $pageBlock = new PageBlock();
        $pageBlock->setBlock($block);
        $pageBlock->setPage($contactPage);
        $pageBlock->setPlace('center');
        $manager->persist($pageBlock);


        $wcagPage = new Page();
        $wcagPage->setId(3);
        $wcagPage->setName('Vyhlásenie o prístupnosti');
        $wcagPage->setTitle('Vyhlásenie o prístupnosti');
        $wcagPage->setLvl(1);
        $wcagPage->setSeoId('vyhlasenie-o-pristupnosti');
        $wcagPage->setContent('
        <h2>Vyhlásenie o prístupnosti</h2>
        <p>Slovenská agentúra životného prostredia má záujem zabezpečiť prístupnosť webového sídla  <a href="">???</a>
            v súlade so zákonom č. 95/2019 Z. z. o informačných technológiách vo verejnej správe a príslušnými vykonávacími predpismi v rozsahu podmienok podľa smernice (EÚ) 2016/2102. Toto vyhlásenie o prístupnosti sa vzťahuje na webové sídlo  <a href="">???</a>
        </p>
        <h3>Stav súladu</h3>
        <p>Toto webové sídlo je v čiastočnom súlade so zákonom č. 95/2019 Z. z. o informačných technológiách vo verejnej správe a s príslušnými vykonávacími predpismi v rozsahu podmienok podľa smernice (EÚ) 2016/2102 vzhľadom na prvky nesúladu uvedené nižšie.</p>

        <h3>Neprístupný obsah</h3>
        <p>Obsah uvedený nižšie nie je prístupný z týchto dôvodov:</p>
        <ol>
            <li>
                Nesúlad so smernicou (EÚ) 2016/2102 o prístupnosti webových sídel a mobilných aplikácií subjektov verejného sektora:
                <ul>
                    <li>???</li>
                </ul>
            </li>
          
        </ol>

        <h3>Vypracovanie tohto vyhlásenia o prístupnosti</h3>
        <p>Toto vyhlásenie bolo vypracované ???.</p>
        <p>Vyhodnotenie súladu webového sídla s požiadavkami zákona č. 95/2019 Z. z. o informačných technológiách vo verejnej správe a príslušnými vykonávacími predpismi v rozsahu podmienok podľa smernice (EÚ) 2016/2102 bolo vykonané samohodnotením.</p>
        <p>Vyhlásenie bolo naposledy skontrolované 24. 3. 2021.</p>

        <h3>Spätná väzba a kontaktné informácie</h3>
        <p>V prípade problémov s prístupnosťou webového sídla nás kontaktujte na e-mailovej adrese: <a href="mailto:">???</a>. Správcom obsahu a prevádzkovateľom webového sídla je Slovenská agentúra životného prostredia.</p>
        <p>Webové sídlo bolo vytvorené na podporu aktivít súvisiacich s prechodom na obehové hospodárstvo v podmienkach Slovenskej republiky a na podporu implementácie princípov zeleného rastu. Obsah webového sídla je pravidelne aktualizovaný.
        </p>
        <h3>Vynucovacie konanie</h3>
        <p>V prípade neuspokojivých odpovedí na podnety alebo žiadosti zaslané v rámci spätnej väzby subjektu verejného sektora v súlade s <a href="https://eur-lex.europa.eu/legal-content/SK/TXT/?uri=uriserv:OJ.L_.2016.327.01.0001.01.SLK&amp;toc=OJ:L:2016:327:TOC">čl. 7 ods. 1 písm. b) smernice Európskeho parlamentu</a> sa môžete obrátiť v rámci vynucovacieho konania na subjekt poverený presadzovaním smernice, ktorým je Ministerstvo investícií, regionálneho rozvoja a informatizácie Slovenskej republiky na e-mailovej adrese: <a href="mailto:standard@vicepremier.gov.sk">standard@vicepremier.gov.sk</a>.</p>');
        $wcagPage->setStatus('public');
        $wcagPage->setLayout('content');
        $wcagPage->setVisibility(true);
        $wcagPage->setParentPage($rootPage);
        $manager->persist($wcagPage);

        $pageBlock = new PageBlock();
        $pageBlock->setBlock($block);
        $pageBlock->setPage($wcagPage);
        $pageBlock->setPlace('center');
        $manager->persist($pageBlock);


        $homePage = new Page();
        $homePage->setName('Domovská stránka');
        $homePage->setTitle('Domov');
        $homePage->setLvl(1);
        $homePage->setSeoId('homepage');
        $homePage->setContent('<section style="min-height:50vh;display:flex; justify-content:center; align-items:center;"><p>Vitaj na domovskej stránke, upraviť ju možeš v <a href="/admin/page/list">redakčnom systeme</a> po prihlasení sa ako admin/admin.</p></section>');
        $homePage->setStatus('public');
        $homePage->setLayout('menu-center');
        $homePage->setVisibility(true);
        $homePage->setParentPage($rootPage);
        $manager->persist($homePage);

        $pageBlock = new PageBlock();
        $pageBlock->setBlock($block);
        $pageBlock->setPage($homePage);
        $pageBlock->setPlace('center');
        $manager->persist($pageBlock);

        $errorPage = new Page();
        $errorPage->setName('Error');
        $errorPage->setTitle('Chyba');
        $errorPage->setLvl(1);
        $errorPage->setSeoId('error');
        $errorPage->setContent('<section style="min-height:50vh;display:flex; justify-content:center; align-items:center;"><p>Stránka sa nenašla</p></section>');
        $errorPage->setStatus('public');
        $errorPage->setLayout('menu-center');
        $errorPage->setVisibility(true);
        $errorPage->setParentPage($rootPage);
        $manager->persist($errorPage);

        $pageBlock = new PageBlock();
        $pageBlock->setBlock($block);
        $pageBlock->setPage($errorPage);
        $pageBlock->setPlace('center');
        $manager->persist($pageBlock);


        $menu = new Menu();
        $menu->setId(1);
        $menu->setName('Hlavné menu');
        $menu->setParentId(0);
        $menu->setLvl(0);
        $manager->persist($menu);

        $manager->flush();

        $menuItemContact = new Menu();
        $menuItemContact->setId(2);
        $menuItemContact->setName('Kontakt');
        $menuItemContact->setRootId($menu->getId());
        $menuItemContact->setParentId($menu->getId());
        $menuItemContact->setLvl(1);
        $menuItemContact->setLinkType('page');
        $menuItemContact->setLinkTypeData('{"id":"' . $contactPage->getId() . '","seoId":"kontakt"}');
        $manager->persist($menuItemContact);


        // FORMULAR
        $formJson = '{"status":"ok","data":{"formId":4,"identifier":"21f742534aa38f44","formName":"Automaticky vygenerovan\u00fd \u00favodn\u00fd formul\u00e1r","formDescription":"\u003Ch2\u003ELorem ipsum dolor sit amet, consectetur adipisicing elit\u003C\/h2\u003E\n\u003Cp\u003EAccusamus aperiam beatae consequuntur dignissimos\u003Cbr\u003E\ndoloribus dolorum eius eos expedita facere magnam necessitatibus non quidem ratione repellendus repudiandae\u003Cbr\u003E\nrerum similique suscipit tempore, vel, velit! Beatae distinctio maxime odio. Consequuntur eligendi explicabo\u003Cbr\u003E\niure magni minima! Aliquid aperiam aut commodi cupiditate error, et explicabo harum id iure modi nemo obcaecati\u003Cbr\u003E\npraesentium quibusdam tenetur veniam. Architecto at doloremque ea, eaque error nulla quas qui tempore vero\u003Cbr\u003E\nvoluptatum. Accusantium autem consequatur incidunt nemo quo rem sint soluta temporibus. Deserunt dignissimos ea\u003Cbr\u003E\nex molestiae possimus repellendus sequi, tempore. Accusamus at commodi consectetur corporis cupiditate deserunt\u003Cbr\u003E\ndistinctio dolorum facere ipsum iste itaque magni neque nihil nostrum officiis placeat possimus, quae quaerat\u003Cbr\u003E\nquia quibusdam quidem quod reprehenderit sapiente sequi similique unde vel.\u003C\/p\u003E","isActive":true,"successMessage":"Diky\n Architecto at doloremque ea, eaque error nulla quas qui tempore vero\n        voluptatum. Accusantium autem consequatur incidunt nemo quo rem sint soluta temporibus. Deserunt dignissimos ea\n        ex molestiae possimus repellendus sequi, tempore. ","visibility":true,"items":[{"identifier":"59m4kwetnvuihmxq","title":"E-mail ","helpText":"Pre dalsiu komunikaciu","type":"email","sortOrder":0,"required":true,"choices":null,"rows":null,"length":null},{"identifier":"5rrqkwgc5vnrol9g","title":"Telef\u00f3nne alebo mobilne \u010d\u00edslo","helpText":"Format: 0944123456 \/ +421 944 123 456","type":"tel","sortOrder":1,"required":true,"choices":null,"rows":null,"length":null},{"identifier":"odlxkwetnwfhcy8v","title":"Po\u010det dn\u00ed do konca roka:","helpText":"ak je pr\u00e1ve posledn\u00fd de\u0148, uve\u010fte 0","type":"number","sortOrder":2,"required":true,"choices":null,"rows":null,"length":null},{"identifier":"nu41kwetnx9voine","title":"D\u00e1tum narodenia najstar\u0161ieho obyvate\u013ea va\u0161ej obce","helpText":"ak si nie ste odpove\u010fou ist\u00fd, obr\u00e1\u0165te sa na obecn\u00fd \u00farad","type":"date","sortOrder":3,"required":true,"choices":null,"rows":null,"length":null},{"identifier":"ydk0kwgc38evy9cn","title":"Meno a priezvisko","helpText":"Odde\u013ete medzerou ","type":"text","sortOrder":4,"required":true,"choices":null,"rows":null,"length":null},{"identifier":"op1ekwgc39mznh1n","title":"Ob\u013e\u00faben\u00fd de\u0148 v t\u00fd\u017edni","helpText":"Ob\u013e\u00faben\u00fd m\u00f4\u017ee byt len jeden de\u0148","type":"select","sortOrder":5,"required":true,"choices":[{"label":"Pondelok","value":"UG9uZGVsb2s=","sortOrder":0,"valid":true},{"label":"Utorok","value":"VXRvcm9r","sortOrder":1,"valid":true},{"label":"Streda","value":"U3RyZWRh","sortOrder":2,"valid":true},{"label":"\u0160tvrtok","value":"xaB0dnJ0b2s=","sortOrder":3,"valid":true},{"label":"Piatok","value":"UGlhdG9r","sortOrder":4,"valid":true},{"label":"Sobota","value":"U29ib3Rh","sortOrder":5,"valid":true},{"label":"Nede\u013ea","value":"TmVkZcS+YQ==","sortOrder":6,"valid":true}],"rows":null,"length":null},{"identifier":"gdu1kwgc3a4qf3uf","title":"Ob\u013e\u00faben\u00e1 z\u00e1kladn\u00e1 farba","helpText":"Ak va\u0161a farba nie je v ponuke uve\u010fte ju do  pozn\u00e1mky","type":"radio","sortOrder":6,"required":true,"choices":[{"label":"Zelen\u00e1","value":"WmVsZW7DoQ==","sortOrder":0,"valid":true},{"label":"\u010cerven\u00e1","value":"xIxlcnZlbsOh","sortOrder":1,"valid":true},{"label":"Modr\u00e1","value":"TW9kcsOh","sortOrder":2,"valid":true},{"label":"In\u00e9","value":"SW7DqQ==","sortOrder":3,"valid":true}],"rows":null,"length":null},{"identifier":"33ywkwgc3ald19hc","title":"\u010co radi rob\u00edte vo vo\u013enom \u010dase","helpText":"","type":"checkbox","sortOrder":7,"required":true,"choices":[{"label":"\u010c\u00edtam","value":"xIzDrXRhbQ==","sortOrder":0,"valid":true},{"label":"Kresl\u00edm","value":"S3Jlc2zDrW0=","sortOrder":1,"valid":true},{"label":"Bicyklujem","value":"QmljeWtsdWplbQ==","sortOrder":2,"valid":true},{"label":"Pot\u00e1pam sa","value":"UG90w6FwYW0gc2E=","sortOrder":3,"valid":true},{"label":"Ni\u010d","value":"TmnEjQ==","sortOrder":4,"valid":true}],"rows":null,"length":null},{"identifier":"s8lekwgc3975om1m","title":"Pozn\u00e1mka","helpText":"\u010co by ste n\u00e1m e\u0161te radi odk\u00e1zali","type":"textarea","sortOrder":8,"required":false,"choices":null,"rows":4,"length":null},{"identifier":"jzfxkwetnyjnp3x6","title":"Hodnotenie","helpText":"Za spr\u00e1vnu odpove\u010f m\u00f4\u017eete dosta\u0165 mal\u00fa pozornos\u0165 ","type":"star","sortOrder":9,"required":true,"choices":null,"rows":null,"length":6}]}}';
        $formArr = json_decode($formJson, true)["data"];
        $form = new Webform();
        $form->setName($formArr["formName"])
            ->setIdentifier($formArr["identifier"])
            ->setDescription($formArr["formDescription"])
            ->setSuccessMessage($formArr["successMessage"])
            ->setIsActive(true)
            ->setVisibility(true);
        $form->setCreatorId(0);
        foreach ($formArr["items"] as $item) {
            $field = new WebformField();
            $form->addField($field);
            $field->setIdentifier($item["identifier"])
                ->setTitle($item["title"])
                ->setHelpText($item["helpText"])
                ->setType($item["type"])
                ->setSortOrder($item["sortOrder"])
                ->setRequired($item["required"])
                ->setVisibility(true);
            if (array_key_exists("choices", $item))
                $field->setChoices($item["choices"]);
            if (array_key_exists("rows", $item))
                $field->setRows($item["rows"]);
            if (array_key_exists("length", $item))
                $field->setLength($item["length"]);

            $manager->persist($field);
        }
        $manager->persist($form);
        $manager->flush();

        $formBlock = new Block();
        $formBlock->setName($form->getName())
            ->setType("form")
            ->setParams(["formId" => $form->getId()]);
        $manager->persist($formBlock);
        $manager->flush();


    }
}
