<?php

//namespace Score\BaseBundle\DataFixtures;
namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Score\CmsBundle\Entity\Group;
use Score\CmsBundle\Entity\User;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();


        $group1 = new Group(); // TODO role index
        $group1->setName('Admin');
        $group1->setRole('ROLE_ADMIN');
        $user->addGroup($group1);
        $manager->persist($group1);

        $group2 = new Group();
        $group2->setName('Redaktor');
        $group2->setRole('ROLE_PAGE_MANAGER');
        $user->addGroup($group2);
        $manager->persist($group2);

        $group3 = new Group();
        $group3->setName('Správca uživateľov');
        $group3->setRole('ROLE_USER_MANAGER');
        $user->addGroup($group3);
        $manager->persist($group3);

        $group4 = new Group();
        $group4->setName('Správca súborov');
        $group4->setRole('ROLE_FILE_MANAGER');
        $user->addGroup($group4);
        $manager->persist($group4);

        $group5 = new Group();
        $group5->setName('Správca galérie');
        $group5->setRole('ROLE_GALLERY_MANAGER');
        $user->addGroup($group5);
        $manager->persist($group5);


        $user->setUsername('admin');
        $user->setEmail('admin@cms3.sk');
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'admin'));
        $user->setIsActive(true);
        $manager->persist($user);
        $manager->flush();
    }
}
